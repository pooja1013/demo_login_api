var fs = require('fs');
module.exports = (req, res) => {
    console.log(req.body)
    var { firstname, lastname, emailId, phoneNumber, pswd, enableFaceId } = req.body
    if (!firstname || firstname == "") {
        return res.status(500).send("Please specify First Name")
    }
    if (!lastname || lastname == "") {
        return res.status(500).send("Please specify Last Name")
    }
    if (!emailId || emailId == "") {
        return res.status(500).send("Please specify Email Id")
    }
    if (!phoneNumber || phoneNumber == "") {
        return res.status(500).send("Please specify Phone Number")
    }
    if (!pswd || pswd == "") {
        return res.status(500).send("Please specify Password")
    }
    if (!enableFaceId || enableFaceId == "") {
        enableFaceId = "false"
    }
    //fetch all user data from file
    fs.readFile('user_data.json', function (err, data) {
        if (err) {
            return res.status(500).send(err.message)
        }
        var existingData = JSON.parse(data)

        console.log("read file", existingData)

        //Check if user already exists
        var ifUserAlreadyFound = false
        for (i in existingData) {
            var record = existingData[i]
            if (record.emailId == emailId || record.phoneNumber == phoneNumber) {
                ifUserAlreadyFound = true
                break
            }
        }
        if (ifUserAlreadyFound) {
            return res.status(409).send("User already exists with this email id or phone number")
        }

        //if user not exists, add in file
        existingData.push({
            "firstname": firstname, "lastname": lastname, "emailId": emailId, "phoneNumber": phoneNumber, "pswd": pswd, "enableFaceId": enableFaceId
        })
        fs.writeFile('user_data.json', JSON.stringify(existingData), function (err) {
            if (err) {
                return res.status(500).send(err.message)
            }
            return res.status(200).send("OK")

        });
    });
}