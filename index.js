const express = require('express');
const bodyParser = require("body-parser");
const signUp = require('./signUp');
const login = require('./login');

const app = express();
const PORT = process.env.PORT || 3000;
const NODE_ENV = process.env.NODE_ENV || 'development';

app.set('port', PORT);
app.set('env', NODE_ENV);
app.set('view engine', 'ejs');


app.use(express.static('public'));
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }))
app.post("/signUp", signUp)
app.post("/login", login)
app.get('/', (req, res) => {
    res.status(200).send("Welcome to Demo Login API");
});

app.listen(PORT, () => {
    console.log("Welcome to Demo Login API")
});
