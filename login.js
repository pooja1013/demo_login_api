var fs = require('fs');
module.exports = (req, res) => {
    console.log(req.body)
    var { username, pswd } = req.body
    if (!username || username == "") {
        return res.status(500).send("Please specify Username")
    }
    if (!pswd || pswd == "") {
        return res.status(500).send("Please specify Password")
    }
    //fetch all user data from file
    fs.readFile('user_data.json', function (err, data) {
        if (err) {
            return res.status(500).send(err.message)
        }
        var existingData = JSON.parse(data)

        //Check if user already exists
        for (i in existingData) {
            var record = existingData[i]
            if (record.emailId == username && record.pswd == pswd) {
                return res.status(200).send("OK")
            }
        }
        return res.status(404).send("Username or password invalid")

    });
}